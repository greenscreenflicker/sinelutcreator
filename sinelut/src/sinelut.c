/*
 ============================================================================
 Name        : sinelut.c
 Author      : Michael  Heidinger
 Version     :
 Copyright   : GPL
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void sinevalsvals(int value, double phase, int linebreak);

int main (int argc, const char * argv[]) {
	if(argc != 4){
		printf("Usage: %s [values] [phase 0..1] [linebreak]\n", argv[0]);
		exit(1);
	}
	int number=atoi(argv[1]);
	if(number>100000){
		printf("Number to big\n");
		exit(1);
	}
	if(number<2){
		printf("Number to small. Must be bigger then 1\n");
		exit(1);
	}

	int linebreak=atoi(argv[3]);
	if(linebreak<1){
		printf("Linebreak to small. Must be bigger then 1\n");
		exit(1);
	}

	float phase;
	phase=atof(argv[2]);

	//printf("float %lf\n", phase);
	if(phase<0 || phase>1){
		printf("Its recommended to keep the phase between 0 and 1.\n");
	}
	sinevalsvals(number,phase,linebreak);

	return EXIT_SUCCESS;
}

void sinevalsvals(int value, double phase, int linebreak){
	printf("{");
	int i;
	for(i=0;i<value;i++){
		if(((i%linebreak)==0)
				&& (i>0)){
			printf("\n");
		}
		//fundamental sine calculus
		double floatingi=i;
		double flatoingvalue=value;
		double result;
		result=sin(2*M_PI*((floatingi/flatoingvalue)+phase));


		printf("%10lf",result);
		if(i!=(value-1)){
			printf("f,");
		}else{
			printf("f");
		}

	}
	printf("}\n");
}
